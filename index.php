<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <?php
        $date = new DateTime("now", new DateTimeZone('Asia/Shanghai') );
        $time = $date->format('H:i');
        ?>

        <title>Beijing News | <?php echo $time;?></title>
        <meta itemprop="name" content="The Beijing Story">
        <meta itemprop="description" content="Latest news headlines and updates from Beijing">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/main.css">

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <center>
        <h3>The Beijing Story</h3>
        <span style="color:yellowgreen">The latest news headlines</span>
        <hr>
        </center>

        <div>
            <?php
            // Create google link plus headline
            function lk_srt($lk, $lbl) {
                return "<a href='$lk' target='_blank'>$lbl</a>";
            }

            // Create date span
            function sn_date($d) {
                return "<span class='date'>$d</span>";
            }

            $string = file_get_contents("data/news.json");
            $json_a = json_decode($string, true);

            foreach ($json_a as $person_name => $nws) {
                        echo lk_srt($nws['srch'], $nws['news']) . '<br>';
                        echo sn_date($nws['date']) . '<br>';
                        echo '<br>';
            }

            ?>
        </div>

        <br><br><br><br><br>
        <div>
            © Copyright by The Beijing Story

        </div>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-86656736-1', 'auto');
            ga('send', 'pageview');

        </script>
    </body>
</html>
